# Monty Hall Problem

The [Monty Hall Problem](https://en.wikipedia.org/wiki/Monty_Hall_problem) is a pedagogical gem in statistics. There exists a number of variants, but the core problem is as follows.

1) There are three (N)[^1] doors. Two of these doors have a goat[^2] behind them, and one of them has a car/money/precious thing behind it. Which door is the winning door is randomly selected.
2) The player selects a door.
3) Monty (the game show host) now has two doors, and you have one. One of Monty's is guaranteed to have a goat, and **Monty knows where the prize is**. He will open one of his two doors, revealing one of the wayward mammals. There are now only two unknown doors.
4) The player is given the opportunity to switch their door to the other door before opening it.

The mathematical question of interest: is it statistically advantageous to switch doors, or is the probability 50:50?

# The code
The Python code provided here requires `numpy` to run. This is an easy problem that can be implemented with only Python's built-in libraries, but `numpy` arrays are convenient to manipulate and make some of the variant Monty Hall problems easier to intuitively grasp.

## `monty_hall.py`
This is the main module. You can run this - The program will prompt you for the number of doors (default 3), whether or not the player should switch doors (default `True`), and how many times to perform the game (default 10000).

If you want to import the function elsewhere, the main Monty Hall routine is encoded in the function `montyHall`.

# Monty Fall Problem
This is an alternative formulation of the problem. Here the game starts exactly the same as normal, until Monty has to reveal a door. This time, instead of using his knowledge of where the prize is to reveal one of the goats, Monty trips and triggers the door opening mechanism. **A door opens at random, revealing a goat.** Is it advantageous to switch doors, or is the probability 50:50?

This situation is simulated by `montyFall`. It takes the same parameters as `montyHall`. Play around with it and see if your intuition needs adjustment.

---

[^1] This code is written to support any number of doors greater than zero. Considering the limiting case where $N \rightarrow \infty$ is a helpful way to see the solution without calculating!

[^2] As I've found while explaining the problem to others, some people actually want a goat. In that case, replace "goat" in the problem statement with "undesirable object".
