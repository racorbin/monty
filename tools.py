import numpy as np

seed = 9541373
RNG = np.random.default_rng(seed)


def sum_ints(N, low=0):
    """Sum the integers from `low` to `N`.

    Parameters
    -----------
    N : int
        Integer to sum to.
    low : int
        Integer to begin sum from. Default is 0.
    """

    if low != 0:
        return sum_ints(N, low=0) - sum_ints(low - 1, low=0)
    else:
        return int(N * (N + 1) / 2)
