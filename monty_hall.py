import numpy as np

from tools import RNG, sum_ints

"""
Simple module for simulating Monty Hall problems.
"""


def montyHall(Ndoors=3, switch=True, runs=1):
    """
    Simulates the traditional Monty Hall problem.
    https://en.wikipedia.org/wiki/Monty_Hall_problem
    Three doors are available, one of which has a prize behind it. The player must choose
    a door, after which Monty opens one of the other doors not containing the prize. The
    player is then given the opportunity to switch which door they have chosen. Is it
    advantageous to switch or to stick with your initial guess?

    Parameters
    ----------
    Ndoors: int
        The number of doors to use in the problem. The default is 3; this is how the
        gameshow was originally set up.
    switch: bool
        Whether or not the player should switch their selected door after an empty door
        is revealed. The default is True, for secret reasons.
    runs: int
        How many runs to perform. Default is 1.

    Returns
    -------
    win_percent: float
        The percentage of runs for which the player chose the door with
        the money/car/goodthing behind it.
    """

    score = 0

    doors = np.zeros([Ndoors, runs]).T
    money_door_index = RNG.integers(Ndoors, size=runs)

    for i in range(runs):
        doors[i][money_door_index[i]] = 1  # this seems dumb
    # advanced indexing

    guess_index = 0  # which door you choose doesn't matter.

    """
    Now you have a guess index and the money door index. There are two cases:
    1) The player chose the money: `money_door_index == guess_index`.
        Throw out all but one random door.
    2) The player chose a goat. Throw out all except `money_door_index` and `guess_index`.
    """
    for i in range(runs):
        if money_door_index[i] == guess_index:
            other_door = (guess_index + 1) % Ndoors
        elif money_door_index[i] != guess_index:
            other_door = money_door_index[i]
        else:
            raise ValueError("Something's wrong with your indexing.")

        # inds = [guess_index, other_door]

        if switch == True:
            guess_index = other_door

        score += doors[i][guess_index]

    win_percent = score / runs
    return win_percent


def montyFall(Ndoors=3, switch=True, runs=1, _verbose=False):
    """
    Simulates the special case Monty Fall problem.
    https://en.wikipedia.org/wiki/Monty_Hall_problem#Variants
    In this case, Monty accidentally opens one of the doors by falling on the mechanism.
    Cases in which Monty accidentally opens the money door are discarded completely.

    Parameters
    ----------
    Ndoors: int
        The number of doors to use in the problem. The default is 3; this is how the
        gameshow was originally set up.
    switch: bool
        Whether or not the player should switch their selected door after an empty door
        is revealed. The default is True, for secret reasons.
    runs: int
        How many runs to perform. Default is 1.

    Returns
    -------
    win_percent: float
        The percentage of runs for which the player chose the door with
        the money/car/goodthing behind it.
    """

    # if Ndoors != 3:
    #   raise ValueError("Ndoors > 3 not currently implemented.
    # Monty would have insurance issues.")

    score = 0
    guess_index = 0  # which door you choose doesn't matter.

    doors = np.zeros([Ndoors, runs]).T
    money_door_index = RNG.integers(Ndoors, size=runs)

    for i in range(runs):
        doors[i][money_door_index[i]] = 1  # this seems dumb

    discarded_runs = 0

    # main routine
    other_door = 1
    inds = [guess_index, other_door]
    assert guess_index != other_door
    doors = doors[:, inds]  # [0, 1]

    for i in range(runs):  # if the discarded door has the car, don't count that run.
        if not np.any(doors[i]):
            discarded_runs += 1
        else:
            if switch == True:
                guess_index = other_door

            score += doors[i][guess_index]

    if score == 0:
        win_percent = 0
    else:
        win_percent = score / (runs - discarded_runs)
    return win_percent


# ==================================
# End of essential functions
# ==================================
def startup_prompt():
    """
    User prompt when this file is run. Asks for a number of doors,
    whether or not to switch your guess after an empty door is discarded,
    and for a number of runs to average over.

    Returns
    -------
    Ndoors, switch, runs : tuple(int, bool, int)
    Tuple to return as unpacked args to montyHall().
    """

    Ndoors = int(input("Number of doors (default is 3): ") or 3)

    if Ndoors <= 0:
        print("Invalid number of doors, defaulting to 3.")
        Ndoors = 3

    switch = True
    _switch = str(input("Do you want to switch doors? (y/n, default y): ") or "y")
    if _switch == "n":  # this doesn't work
        switch = False

    runs = int(input("Number of runs (default 1000): ") or 1000)

    return Ndoors, switch, runs


def main():
    """
    Main routine for this module. Runs the startup prompt and returns the
    win percentage of the traditional Monty Hall problem based on these params.
    """
    args = startup_prompt()
    m = montyHall(*args)

    print(f"Wins: {m}%")


if __name__ == "__main__":
    main()
