import numpy as np

from tools import RNG, sum_ints

"""
Alternative module for Monty Hall problems, utilizing classes instead of pure functions.
Ultimately this didn't help me reduce code duplication, as the main routine of both problems is short and the number of elements in common doesn't really justify the inheritance scheme. Use `monty_hall.py` instead.
"""


class MontyHall:
    """
    Simulates the traditional Monty Hall problem.
    https://en.wikipedia.org/wiki/Monty_Hall_problem
    Three doors are available, one of which has a prize behind it. The player must choose
    a door, after which Monty opens one of the other doors not containing the prize. The
    player is then given the opportunity to switch which door they have chosen. Is it
    advantageous to switch or to stick with your initial guess?

    Parameters
    ----------
    Ndoors: int
        The number of doors to use in the problem. The default is 3; this is how the
        gameshow was originally set up.
    switch: bool
        Whether or not the player should switch their selected door after an empty door
        is revealed. The default is True, for secret reasons.
    runs: int
        How many runs to perform. Default is 1.

    Returns
    -------
    win_percent: float
        The percentage of runs for which the player chose the door with
        the money/car/goodthing behind it.
    """

    guess_index = 0

    def __call__(self, Ndoors=3, switch=True, runs=1, **kwargs):
        """This class implementation doesn't save much space."""

        doors = np.zeros([runs, Ndoors])
        money_door_index = RNG.integers(Ndoors, size=runs)

        for i in range(runs):
            doors[i][money_door_index[i]] = 1  # this seems dumb
            # advanced indexing

        percent_won = self.main(
            doors=doors, money_door_index=money_door_index, switch=switch
        )

        return percent_won

    def main(self, doors, money_door_index, switch):

        """
        Now you have a guess index and the money door index. There are two cases:
        1) The player chose the money: `money_door_index == guess_index`.
            Throw out all but one random door.
        2) The player chose a goat. Throw out all except
            `money_door_index` and `guess_index`.
        """
        runs, Ndoors = doors.shape
        score = 0

        for i in range(runs):
            if money_door_index[i] == self.guess_index:
                other_door = (self.guess_index + 1) % Ndoors
            elif money_door_index[i] != self.guess_index:
                other_door = money_door_index[i]
            else:
                raise ValueError("Something's wrong with your indexing.")

            guess_index = self.guess_index

            if switch == True:
                guess_index = other_door

            score += doors[i][guess_index]

        return score / runs


class MontyFall(MontyHall):
    """
    Simulates the special case Monty Fall problem.
    https://en.wikipedia.org/wiki/Monty_Hall_problem#Variants
    In this case, Monty accidentally opens one of the doors by falling on the mechanism.
    Cases in which Monty accidentally opens the money door are discarded completely.

    Parameters
    ----------
    Ndoors: int
        The number of doors to use in the problem. The default is 3; this is how the
        gameshow was originally set up.
    switch: bool
        Whether or not the player should switch their selected door after an empty door
        is revealed. The default is True, for secret reasons.
    runs: int
        How many runs to perform. Default is 1.

    Returns
    -------
    win_percent: float
        The percentage of runs for which the player chose the door with
        the money/car/goodthing behind it.
    """

    def main(self, doors, money_door_index, switch, _verbose=False):

        score = 0
        discarded_runs = 0
        runs, Ndoors = doors.shape

        # main routine
        other_door = 1
        inds = [self.guess_index, other_door]
        assert self.guess_index != other_door
        doors = doors[:, inds]  # [0, 1]

        for i in range(
            runs
        ):  # if the discarded door has the car, don't count that run.
            if not np.any(doors[i]):
                discarded_runs += 1
            else:
                guess_index = self.guess_index
                if switch == True:
                    guess_index = other_door

                score += doors[i][guess_index]

        if score == 0:
            res = 0
        else:
            res = score / (runs - discarded_runs)
        return res


montyHall = MontyHall()
montyFall = MontyFall()
